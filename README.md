# git-flow-kata


La idea de esta kata es poder aprender las bases de git flow. Para eso, se deberá realizar la siguiente ejercitación con los siguientes constraints.

### Ejercitacion

#### Iteracion 1

Estamos en el boom de los hipercasual, y necesitamos un exito para poder cumplir nuestro sueño de vivir en una mansión de lujo. Para eso, vamos a crear un clicker para matar a un enemigo, el cuál se llamará Clicker to Die. La idea es que nos aparecerá un enemigo y tendremos que apretar un botón para atacarlo y quitarle vida. Cuando lo matemos, podremos volver a hacerlo aparecer apretando un botón y su vida volverá a la máxima.

Entonces, consta de tres features:

1. Mostrar la vida y el nombre del enemigo al empezar
2. Cada vez que se apreta el botón de atacar deberá quitarle vida.
3. Si se apreta el botón de reset deberá volver a tener toda la vida.

#### Iteracion 2

Hemos publicado versión y nos dimos cuenta que el nombre que le pusimos al enemigo hace que crashee en los 80% de los devices. Tenemos que hacer un hotfix ya y cambiarle el nombre a "John Leyner"

_Nota: Siendo un hotfix, tener en cuenta cómo debería ser fixeado siguiendo GitFlow_

#### Retrospective

Enhorabuena! Pudimos lanzar nuestro primer hipercasual y el juego es un exito!

Para verificar que cumplimos correctamente GitFlow, tenemos que ir al gráfico (Repository->Graph). ¿Quedó similar a los gráficos que se usan de ejemplo para representar GitFlow?

### Constraints

- No se puede crear código productivo sin TDD.
- Se tiene que hacer un commit y pushear por cada test creado/modificado. Nada de commits con muchos cambios.
- Master(branch "main") solo puede tener una versión final.
- Cada feature deberá ser trabajada en un branch, y una vez terminada deberá ser mergeada a develop con un merge request de Gitlab.

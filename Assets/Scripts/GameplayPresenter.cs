public class GameplayPresenter
{
    private readonly IGameplayView _gameplayView;

    public GameplayPresenter(IGameplayView gameplayView)
    {
        _gameplayView = gameplayView;
    }
}
using UnityEngine;

public class Character
{
    private readonly string _name;
    private float _life;

    public Character()
    {
        _name = "Michael Scott";
        _life = 100;
    }

    public void ReceiveDamage()
    {
        _life = Mathf.Clamp(_life - 10, 0, 100);
    }

    public string GetName() => _name;
    public float GetLife() => _life;
}
﻿using UnityEngine;

public class Context : MonoBehaviour
{
    public GameplayView gameplayView;
    
    void Start()
    {
        var character = new Character();
        var presenter = new GameplayPresenter(gameplayView);
    }
}